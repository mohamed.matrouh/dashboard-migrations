CREATE STREAM users WITH (
    kafka_topic = 'postgres.public.owner',
    value_format = 'avro'
);

CREATE TABLE USERS_TABLE (ID INTEGER PRIMARY KEY, username VARCHAR) WITH (
    kafka_topic = 'postgres.public.owner',
    value_format = 'avro'
);

CREATE TABLE users_count 
  AS SELECT ID, username 
  FROM USERS_TABLE 
  WHERE username= 'seth.ebert';