CREATE TABLE campaign (id BIGINT primary key, name VARCHAR, type Varchar, owner_id BIGINT )
WITH (KAFKA_TOPIC='postgres.public.campaign',KEY_FORMAT='JSON',
partitions = 1,
VALUE_FORMAT='AVRO');

CREATE TABLE campaign_view 
  AS SELECT *
  FROM campaign;


CREATE TABLE campaign_status
  AS SELECT ca.type,count(*) as total_campaign
  FROM campaign ca group by ca.type;

select * from campaign_view cv where cv.OWNER_ID=4 ;